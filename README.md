# File Counter

This Ruby script allows you to count the number of files with the same content within a specified directory. It can handle cases where there are multiple files with the same content and provides informative output about the contents with the highest count.

## Usage
1. Clone the repository:

```
git@gitlab.com:ahmadsyaripramdani/file-counter.git
```
2. Navigate to the directory containing the script:
```
cd file-counter
```
3. Run the script with the following command, providing the path to the directory you want to scan:
```
ruby file_counter.rb /path/to/directory
```
Replace /path/to/directory with the directory you want to scan.

4. The script will output the content(s) with the highest count and the number of files with that content. If there are multiple contents with the same count, it will display all of them.

## Example
how the script output would look for a scenario where there are 4 files with content "abcd" and 5 files with content "abcdef":

```
There are 5 files for "abcdef" content
```

### Improvement.
Suppose you have a directory containing the following files:

- 4 files with content "abcd"
- 4 files with content "abcdef"

Running the script will output:
```
There are 4 files for "abcd" content
There are 4 files for "abcdef" content
```

## Test
Run the script with the following command
```
ruby file_counter_tets.rb
```

## Author
Ahmad Ramdani
