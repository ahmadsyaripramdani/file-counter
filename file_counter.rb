require 'digest'

def count_files_with_same_content(path)
  file_contents = Hash.new(0)

  Dir.glob("#{path}/**/*") do |file|
    next unless File.file?(file)

    content = File.read(file)
    content_hash = Digest::SHA256.hexdigest(content)
    file_contents[content] += 1
  end

  max_count = file_contents.values.max
  max_contents = file_contents.select { |_, count| count == max_count }.keys

  return max_contents, max_count
end

def main
  if ARGV.empty?
    puts "Usage: ruby file_counter.rb <directory_path>"
    exit 1
  end

  directory_path = ARGV[0]

  max_contents, max_count = count_files_with_same_content(directory_path)

  if max_count.nil?
    puts "No files found."
  else
    if max_contents.size > 1
      max_contents.each do |content|
        puts "There are #{max_count} files for \"#{content.strip}\" content"
      end
    else
      puts "There are #{max_count} files for \"#{max_contents.first.strip}\" content"
    end
  end
end

main if __FILE__ == $PROGRAM_NAME
