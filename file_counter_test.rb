require 'minitest/autorun'
require_relative 'file_counter'

class TestFileCounter < Minitest::Test
  def setup
    # Create temporary directory and files for testing
    @test_dir = Dir.mktmpdir
    @abcd_files = create_files_with_content('abcd', 4, @test_dir)
    @abcdef_files = create_files_with_content('abcdef', 5, @test_dir)
  end

  def teardown
    # Remove temporary directory and files after testing
    FileUtils.remove_entry(@test_dir)
  end

  def test_count_files_with_same_content
    max_contents, max_count = count_files_with_same_content(@test_dir)

    assert_equal ['abcdef'], max_contents
    assert_equal 5, max_count
  end

  private

  def create_files_with_content(content, count, directory)
    files = []
    count.times do |i|
      file_path = File.join(directory, "file_#{i}.txt")
      File.write(file_path, content)
      files << file_path
    end
    files
  end
end
